# 11ty

Eleventy [Documentation](https://www.11ty.dev/docs/ "offical 11ty documentation")

## Getting Started

If you have have not already cloned the *Campaign Landing Pages* [BitBucket Repo](https://bitbucket.org/scudderuhsinc/campaign-landing-pages/src/main/ "current development repo"), do so. Next you need to install all the required node packages to your local copy of the repo.

## Build local

To build the static websites locally, you will need to:

1. Start your local Sanity Studio. Navigate into the `studio` directory, using the command line interface (CLI) enter:

`/studio $` `sanity start`

2. To build your local static websites, navigate to the `eleven-ty` directory, and with the CLI enter:

`/eleven-ty $` `npx @11ty/eleventy`

you will be given a link to open in your favorate web browser.