# Sanity Studio

Sanity [Documentation](https://www.sanity.io/docs/overview-introduction "official Sanity docmentation")

## Getting Started

If you have have not already cloned the *Campaign Landing Pages* [BitBucket Repo](https://bitbucket.org/scudderuhsinc/campaign-landing-pages/src/main/ "current development repo"), do so. Next you need to install all the required node packages to your local copy of the repo.

## Build local

To run a local copy of the project's Sanity Studio, use the command line interface (CLI) to navigate to the `studio` directory. Then enter

`/studio $` `sanity start`

you will be given a link to open in your favorate web browser.

## Resources
Sanity.io - Page Builder [tutorial](https://www.sanity.io/guides/how-to-use-structured-content-for-page-building "reference for creatind design components")[studio repo](https://github.com/sanity-io/sanity-template-kitchen-sink/tree/master/template/studio "Kitchen Sink - Sanity Studio")